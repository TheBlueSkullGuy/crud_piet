package punk;

public interface iConnection_jdbc {
    void create_connection();
    void check_status();
    void delete_connection();
}
