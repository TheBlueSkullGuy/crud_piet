package punk;

public interface iCRUD_jdbc {
    void create_data();
    void read_data();
    void update_data();
    void delete_data();
}
