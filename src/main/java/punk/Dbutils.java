package punk;

import java.sql.*;

public class Dbutils {
    private String dburl, username, password, query;
    private Dbtype dbtype;
    private Connection dbconnection = null;
    private Statement stmtdb = null;

    public Dbutils(String dburl, String username, String password, String query, Dbtype dbtype) {
        this.dburl = dburl;
        this.username = username;
        this.password = password;
        this.query = query;
        this.dbtype = dbtype;
    }
    public void factory () {
        switch (dbtype){
            case RDBMS:
                rdbms Rdbms = new rdbms();
                break;
            case MONGODB:
                mongo Mongo = new mongo();
                break;
            case CASSANDRA:
                cassandra Cassandra = new cassandra();
                break;

        }
    }
        public void connectiondb(){
            try {
                dbconnection= DriverManager.getConnection(dburl, username, password);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                stmtdb = dbconnection.createStatement();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            ResultSet rs;
            try {
                rs = stmtdb.executeQuery(query);

                while (rs.next()) {
                    System.out.println(rs.getString("Doctor_name"));
                }
            } catch (SQLException e) {
                e.printStackTrace();

            }


            }
        }


